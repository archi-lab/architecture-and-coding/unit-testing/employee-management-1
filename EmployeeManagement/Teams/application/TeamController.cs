﻿using Microsoft.AspNetCore.Mvc;
using EmployeeManagement.Teams.domain;

namespace EmployeeManagement.Teams.application
{
    [ApiController]
    [Route("[controller]")]
    public class TeamController : ControllerBase
    {
        private readonly ITeamRepository _TeamRepository;

        public TeamController(ITeamRepository TeamRepository)
        {
            _TeamRepository = TeamRepository;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Team>>> GetAll()
        {
            var units = await _TeamRepository.GetAll();
            return Ok(units);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Team>> GetById(Guid id)
        {
            var unit = await _TeamRepository.Get(id);

            if (unit == null)
            {
                return NotFound();
            }

            return Ok(unit);
        }



        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            var unit = await _TeamRepository.Get(id);
            if (unit == null)
            {
                return NotFound();
            }

            await _TeamRepository.Delete(id);
            return NoContent();
        }
    }
}
