﻿using EmployeeManagement.Employees.application;
using EmployeeManagement.Employees.domain;
using System;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;

namespace EmployeeManagement.Employees.application
{

    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly HttpClient _httpClient;

        public EmployeeService(IEmployeeRepository employeeRepository, HttpClient httpClient)
        {
            _employeeRepository = employeeRepository;
            _httpClient = httpClient;
        }

        /// <summary>
        /// Query an employee including the phone extension, which is obtained from an external
        /// EmployeeManagement service
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Employee> QueryFull(Guid id)
        {
            var employee = await _employeeRepository.Get(id);
            if (employee == null) return null;

            var enrichedEmployee = await EnrichEmployeeData(employee);
            return enrichedEmployee;
        }


        /// <summary>
        /// Query all employees including the phone extension, which is obtained from an external
        /// service, and sorted alphabetically
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Employee>> QueryAllFullAndSorted()
        {
            var employees = await _employeeRepository.GetAllSorted();
            var enrichedEmployees = new List<Employee>();
            foreach (var employee in employees)
            {
                var enrichedEmployee = await EnrichEmployeeData(employee);
                enrichedEmployees.Add(enrichedEmployee);
            }
            return enrichedEmployees;
        }



        private async Task<Employee> EnrichEmployeeData(Employee employee)
        {
            // Query the JSONPlaceholder API for the user with the given name
            string name = employee.Name.Replace(" ", "%20");
            var response = await _httpClient.GetAsync($"https://jsonplaceholder.typicode.com/users?name={name}");
            if (!response.IsSuccessStatusCode) return employee;

            var jsonArray = await response.Content.ReadAsStringAsync();
            if (jsonArray == null) return employee;
            List<JsonPlaceholderUser> people = JsonSerializer.Deserialize<List<JsonPlaceholderUser>>(jsonArray);
            if (people == null || people.Count == 0 || people.Count > 1) return employee;

            // Enrich the employee data with information from the JSONPlaceholder API
            employee.Phone = people[0].Phone;

            return employee;
        }


        public async Task<List<Employee>> GetSubordinates(Employee employee)
        {
            return await _employeeRepository.FindEmployeesReportingTo(employee.Id);
        }



        public async Task<List<EmployeeTreeViewDto>> QueryAllEmployeesAsTreeViewDtos()
        {
            var directors = await _employeeRepository.FindCEOs();
            var directorDtos = new List<EmployeeTreeViewDto>();

            foreach (var ceo in directors)
            {
                var directorTreeViewDto = await GetEmployeeTreeViewDto(ceo);
                directorDtos.Add(directorTreeViewDto);
            }

            return directorDtos;
        }


        private async Task<EmployeeTreeViewDto> GetEmployeeTreeViewDto(Employee employee, int hierarchyLevel = 0)
        {
            var employeeTreeViewDto = new EmployeeTreeViewDto
            {
                Id = employee.Id,
                Name = employee.Name,
                Role = employee.Role,
                PayGrade = employee.PayGrade,
                HierarchyLevel = hierarchyLevel,
                Children = new List<EmployeeTreeViewDto>()
            };

            var subordinates = await GetSubordinates(employee);
            foreach (var subordinate in subordinates)
            {
                var childTreeViewDto = await GetEmployeeTreeViewDto(subordinate, hierarchyLevel + 1);
                employeeTreeViewDto.Children.Add(childTreeViewDto);
            }

            return employeeTreeViewDto;
        }


    }



    public class JsonPlaceholderUser
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("phone")]
        public string Phone { get; set; }
    }

}
