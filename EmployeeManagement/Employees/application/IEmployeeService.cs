﻿using EmployeeManagement.Employees.application;
using EmployeeManagement.Employees.domain;

namespace EmployeeManagement.Employees.application
{
    public interface IEmployeeService
    {
        public Task<Employee> QueryFull(Guid id);
        public Task<IEnumerable<Employee>> QueryAllFullAndSorted();
        public Task<List<EmployeeTreeViewDto>> QueryAllEmployeesAsTreeViewDtos();
    }
}
