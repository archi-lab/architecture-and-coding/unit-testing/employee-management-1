﻿using Microsoft.EntityFrameworkCore;
using EmployeeManagement.Config;

namespace EmployeeManagement.Employees.domain
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly EmployeeManagementContext _context;

        public EmployeeRepository(EmployeeManagementContext context)
        {
            _context = context;
        }

        public async Task<Employee> Get(Guid id)
        {
            return await _context.Employees.FindAsync(id);
        }

        public async Task<IEnumerable<Employee>> GetAllSorted()
        {
            return await _context.Employees.OrderBy(e => e.Name).ToListAsync();
        }

        public Task<List<Employee>> FindEmployeesReportingTo(Guid superiorId)
        {
            var employeesReportingTo = _context.Employees.Where(e => e.Superior != null && e.Superior.Id == superiorId)
                    .ToListAsync();
            return employeesReportingTo;
        }

        public async Task<List<Employee>> FindCEOs() => await _context.Employees.Where(e => e.Superior == null).ToListAsync();

        public async Task Add(Employee employee)
        {
            await _context.Employees.AddAsync(employee);
            await _context.SaveChangesAsync();
        }

        public async Task Update(Employee employee)
        {
            _context.Entry(employee).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var employee = await _context.Employees.FindAsync(id);
            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();
        }
    }
}
